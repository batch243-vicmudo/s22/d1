// Array methods

// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array elements.

//[Section] Mutator Methods
	//Mutator methods are functions that mutate or change an array after they're created.
	//These methods manipulate the original array performing various tasks as adding and removing elements.

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];

console.log(fruits);

		// push ()
		/*
			-Adds an element/s in the end of an array and returns the array's length.
			-Syntax:
			arrayName.push(elementsToBeAdded);
		*/
	console.log("Current Array fruits[]: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');
	console.log("Mutated array from push method: ");
	console.log(fruits);
	console.log(fruitsLength);

	//Adding multiple elements to an array.
	fruitsLength = fruits.push("Avocado", "Guava");
	console.log("Mutated array from push method: ");
	console.log(fruits);
	console.log(fruitsLength);

	// pop()
	/*
		-removes the last element in an array AND returns the removed element
		Syntax:
		arrayName.pop();
	*/
	console.log("----------------------------------------------");
	console.log("Current Array fruits[]: ");
	console.log(fruits);

	let removedFruit = fruits.pop();
	console.log("Mutated Array after the pop method: ");
	console.log(fruits);
	console.log(removedFruit);
	console.log("----------------------------------------------");

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	removedFruit = fruits.pop();
	console.log("Mutated Array after the pop method: ");
	console.log(fruits);
	console.log(removedFruit);
	console.log("----------------------------------------------");

	// unshift()
	/*
		-adds one or more elements at the beginning of an array AND returns the present length.
		-Syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB', . . . );
	*/

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	let fruitsLengths = fruits.unshift('Lime', 'Banana');
	console.log("Mutated Array after the unshift() method: ");
	console.log(fruits);
	console.log(fruitsLengths);
	console.log("---------------------------------------------");

	// shift()
	/*
		-removes an element at the beginning of an array AND it returns the removed element.
		-Syntax:
			arrayName.shift()
	*/ 
	console.log("Current Array fruits[]: ");
	console.log(fruits);

	returnFruit = fruits.shift();
	console.log("Mutated Array after the shift() method: ");
	console.log(fruits);
	console.log(returnFruit);
	console.log("---------------------------------------------");

	// splice()
		/*Simultaneously removes elements from a specified index number and adds elements.
		-Syntax:
			arrayName.splice(startingIndex,deleteCount, elementsToBeAdded)
	*/
	console.log("----------------------------------------------");

	console.log("Current Array fruits[]: ");
	console.log(fruits);

	let returnFruit1 =fruits.splice (1, 1, "Lime");
	console.log("Mutated Array after the splice() method: ");
	console.log(fruits);
	console.log("---------------------");
	let index = 3;
	console.log(fruits);
	fruits.splice(index, 1);
	console.log(fruits);
	console.log("---------------------");
	console.log("Current Array fruits[]: ");
	console.log(fruits);

	fruits.splice (3, 0, "Durian", "Santos", "Star Apple");
	console.log("Mutated Array after the splice() method: ");
	console.log(fruits);
	console.log(returnFruit1);
	console.log("----------------------------------------------");

	// sort()
	/*
		-Rearranges the array elements in alphanumeric order
		-Syntax:
		arrayName.sort();
	*/
	console.log("Current Array fruits[]: ");
	console.log(fruits);
	returnFruits = fruits.sort();

	console.log("Mutated Array after the sort() method: ");
	console.log(fruits);
	console.log(returnFruits);
	/*
		Important Note 
		The sort method is used for more complicated functions focus the bootcampers on the basic usage of the sort method.
	*/
	console.log("----------------------------------------------");

	//Reverse()
	/*
		-reverses the order of array elements
		syntax:
		arrayName .reverse();
	*/
	console.log("Current Array fruits[]: ");
	console.log(fruits);
	
	returnFruits = fruits.reverse();

	console.log("Mutated Array after the reverse() method: ");
	console.log(fruits);
	console.log(returnFruits);
	console.log("----------------------------------------------");

	// [Section] Non-mutator Methods
	/*
		-Non-mutator methods are functions that do not modify or change an array after they're created.
		-These methods do not manipulate the original array permforming various tasks such as returning elements from an array and combining arrays and printing the output.
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];
	// indexOF()
	/*
		-it returns the index number of the first matching element found in an array
		-if no match was found, the result will be -1
		-The search process will be done from first element proceeding to the last element.
		Syntax: arrayName.indexOf(searchValue);
				arrayName.indexOf(searchValue, startingIndex);
	*/

	console.log(countries.indexOf('PH'));
	console.log(countries.indexOf('BR'));

	//arrayName.indexOf(searchValue, startingIndex);
	console.log(countries.indexOf('PH', 2));

	//lastIndexOf()
	/*
		-returns the index number of the last matching element found in an array
		-the search process will be done from last element proceeding to the first element.
		-Syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, startingIndex);
	*/

	console.log(countries.lastIndexOf('PH'));

	// slice()
	/*
		-portion/slices from an array and returns a new array
		-Syntax
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
	*/
	//Slicing off elements from a specified index to the last element
	let sliceArrayA = countries.slice(2);
	console.log(sliceArrayA);
	console.log(countries);

	// Slicing off elements from a specified index to another index.
	let sliceArrayB = countries.slice(1, 5);
	console.log(sliceArrayB);

	//Slicing off elements starting from the last element of an array
	let sliceArrayC = countries.slice(-3, -1);
	console.log(sliceArrayC);

	// toString()
	/*
		-returns an array as string separated by commas
		-Syntax
		arrayName.toString().
	*/
	let stringArray = countries.toString();
	console.log(stringArray);

	//concat()
	/*
		combines arrays and returns the combined result
		-Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
	*/
	console.log("----------------------------------------------");

	let tasksArrayA = ['drink HTML', 'eat javascript'];
	let tasksArrayB = ['inhale CSS', 'breathe SASS'];
	let tasksArrayC = ['get git', 'be node'];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(`Result from concat method: `);
	console.log(tasks);

	//combining multiple arrays
	console.log("Result from concat method: ");
	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks);

	// combine arrays with elements
	let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
	console.log("Result form concat method: ");
	console.log(combinedTasks);

	//join()
	/*
		-returns an array as string separated by specified separator string
		-syntax:
		arrayName.join('separatorString');
	*/

	let users = ['John', 'Joe', 'Jane', 'Robert'];

	console.log(users.join());
	console.log(users.join(' '));
	console.log(users.join(' - '));

	// [Section] Iteration Methods

	/*
		Iteration methods are loop designed to perform repetitive tasks on arrays
		Iteration method loops over all elements in array.
	*/

	// forEach()
	/*
		-similar to a for loop that iterates on each of array element
		-for each element in the array, the function in the foreach method will be run.
		-Syntax:
		arrayName.forEach(function(indivualElement){
			statement/s.
		})
	*/
	console.log("-----------------------------------------------");
	console.log(allTasks);

	allTasks.forEach(function(task){
		console.log(task);
	})

	let filteredTasks = [];

	let task = allTasks.forEach(function(task){
		if(task.length>10){
			filteredTasks.push(task);
		}
	})
	console.log(task);
	console.log(filteredTasks);
console.log("-----------------------------------------------")
	//map()
	/*
		-iterates on each element and returns new array with different values depending on the result of the function's operation.
		-Syntax:
			let/const resultArray = arrayName.map(function(elements){
				statements;
				return
			})
	*/
	let numbers = [1,2,3,4,5];
	let numberMap = numbers.map(function(number){
		return number*number;
	})

	console.log("Original Array: ");
	console.log(numbers);

	console.log("Result of map method");
	console.log(numberMap);
console.log("-----------------------------------------------");

// every()
/*
	checks if all elements in an array meet the given condition
	-this is useful validating data stored in arrays. Especially when dealing with large amounts of data.
	-returns a true value if all elements meet the condition and false if otherwise.
	-Syntax:
	let/const resultArray = arrayName.every(function(element){
	return expression/condition;
	})
*/

	console.log(numbers);
	let allValid = numbers.every(function(number){
		return(number <3);
	})

	console.log(allValid);
console.log("-----------------------------------------------");

// some()
/*
	-checks if at least one element in the array meets the given condition;
	-returns a true value if at least one element meets the condition and false if none.
	-Syntax:
		let/const resultArray = arrayName.some(function(elements){
		return expression/condition;
		})
*/

console.log(numbers);
let someValid = numbers.some(number => {
	return(number < 2);
})
console.log(someValid);
console.log("-----------------------------------------------");

//filter()
/*
	-return new array that contains the elements which meets the given condition.
	-return an empty array if no element/s were found.
	-Syntax:
	let/const resultArray = arrayName.filter(function(element){
	return expression/condition;
	})
*/

console.log(numbers);
let filterValid = numbers.filter(function(number){
	return(number < 3);
})
console.log(filterValid);

let filterValid1 = numbers.filter(function(number){
	return(number === 0);
})
console.log(filterValid1);
console.log("-----------------------------------------------");

//includes()
/*
	-includes() checks if the argument passed can be found in the array.
	-it returns boolean which can be save in variable 
		-returns true if the argument is found in the array
		-returns false if it is not.
	-Syntax:
		arrayName.includes(argument);
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound = products.includes('Mouse');
console.log(productFound);

let productFound2 = products.includes('Headset');
console.log(productFound2);
console.log("-----------------------------------------------");
//reduce();
/*
	-evaluate elements from left to right and returns/reduces the array into a single value
	-Syntax:
	let/const resultValue = arrayName.reduce(function(accumulator, currentValue){
	return expression/operation
	})
*/

console.log(numbers);
let total = numbers.reduce(function(x,y){
	console.log("this is the value of x: " +x );
	console.log("this is the value of y: "+y);
	return x + y;
})
console.log("----------");
console.log("Total of all number: ");
console.log(total);